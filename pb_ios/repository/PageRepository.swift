//
//  PageRepository.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation

protocol PageRepository {
    func getPage(pageId: String, ifSuccess: @escaping (Page) -> Void, ifError:@escaping (String) -> Void)
    func getPublicPages( ifSuccess: @escaping (Array<Page>) -> Void, ifError: @escaping (String) -> Void) 

}
