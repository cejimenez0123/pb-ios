//
//  UserRepository.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation


protocol UserRepository {
    func getUser(uId: String, ifSuccess: @escaping (User) -> Void)
    func getProfile(profileId: String, ifSuccess: @escaping (Profile) -> Void, ifError:@escaping (String) -> Void)
}
