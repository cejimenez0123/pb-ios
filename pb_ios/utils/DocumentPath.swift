//
//  DocumentPath.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation

 struct DocumentPath {
    static let user = "user"
    static let page = "page"
    static let book = "book"
    static let library = "library"
    static let profile = "profile"
    
}
