//
//  ViewModelFactory.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation
import FirebaseFirestore
class ViewModelFactory:ObservableObject{
    private let pbDatabase = Firestore.firestore()
    let userRepository:UserRepository
    let pageRepository:PageRepository
    init() {
        self.userRepository = UserRepositoryImpl(pbDatabase: pbDatabase)
        self.pageRepository = PageRepositoryImpl(pbDatabase: pbDatabase)
    }
    func makeDashboardViewModel()->DashboardViewModel{
      let userRepository = UserRepositoryImpl(pbDatabase: pbDatabase)
        let pageRepository  = PageRepositoryImpl(pbDatabase: pbDatabase)
        return DashboardViewModel(mainRepo: userRepository,pageRepo: pageRepository)
    }
}
