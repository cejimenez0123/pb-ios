//
//  UserRepositoryImpl.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

class UserRepositoryImpl:UserRepository{
   
    
    let decoder = JSONDecoder()
   
    private var pbDatabase:Firestore
    init(pbDatabase: Firestore) {
        self.pbDatabase = pbDatabase
    }
    func getUser(uId: String, ifSuccess: @escaping (User) -> Void) {
        pbDatabase.collection(DocumentPath.user).document(uId).getDocument(){
            (document,e) in
            if let document = document, document.exists {
                
                let data = document.data()
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data!, options: [])
                    let user = try JSONDecoder().decode(User.self, from: jsonData)
                    ifSuccess(user)
                } catch {
                    print(error.localizedDescription)
                }
                
            }}}
    
    
    func getProfile(profileId: String, ifSuccess: @escaping (Profile) -> Void, ifError:@escaping (String) -> Void) {
        pbDatabase.collection(DocumentPath.profile).document(profileId).getDocument(){
            (document,e) in
            if e != nil{
                ifError(e!.localizedDescription)
            }
            
            if let document = document, document.exists {
               
                let data = document.data()
                do {
                    let profile = document.data()
//                    let jsonData = try JSONSerialization.data(withJSONObject: data!, options: [])
////                    let profile = try JSONDecoder().decode(Profile.self, from: jsonData)
                    ifSuccess(profile)
                } catch {
                    print(error.localizedDescription)
                }
                
            }}}
    
}

extension QueryDocumentSnapshot {
    func decoded<Type: Decodable>() throws -> Type {
        let jsonData = try JSONSerialization.data(withJSONObject: data(), options: [])
        let object = try JSONDecoder().decode(Type.self, from: jsonData)
        return object
    }
}

extension QuerySnapshot {
    func decoded<Type: Decodable>() throws -> [Type] {
        let objects: [Type] = try documents.map({try $0.decoded() })
        return objects
    }
}

