//
//  PageRepositoryImpl.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import Foundation
import FirebaseFirestore

class PageRepositoryImpl:PageRepository{
   
    
    private var pbDatabase:Firestore
    init(pbDatabase: Firestore) {
        self.pbDatabase = pbDatabase
    }
    func getPage(pageId: String, ifSuccess: @escaping (Page) -> Void, ifError: @escaping (String) -> Void) {
        pbDatabase.collection(DocumentPath.page).document(pageId).getDocument(){
            (document,e) in
            if let document = document, document.exists {
                
                let data = document.data()
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data!, options: [])
                    let page = try JSONDecoder().decode(Page.self, from: jsonData)
                    ifSuccess(page)
                } catch {
                    print(error.localizedDescription)
                }
                
            }}}
    
    func getPublicPages( ifSuccess: @escaping (Array<Page>) -> Void,ifError:  @escaping (String) -> Void) {
        pbDatabase.collection(DocumentPath.page).whereField("privacy", isEqualTo: false).getDocuments(completion: {
        (documents,e) in
        
        if e != nil {
            ifError(e!.localizedDescription)
        }
        if let document = documents, !(documents?.isEmpty ?? true){
            
                let pages:Array<Page> = try! document.decoded()
                       
               
                ifSuccess(pages)
            
            
        }
    })
}
    
    
}
