//
//  Page.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import Foundation
import FirebaseFirestoreSwift

struct Page:Codable,Identifiable{
   @DocumentID var id:String?
    var data:String
    var title:String
    var type:String
    var profileId:String
    var approvalScore:Double
    var privacy:Bool
    

}

enum PageType {
   static let text = "html/text"
   static let picture = "image"
    
}
