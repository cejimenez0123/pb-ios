//
//  Book.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import Foundation
import FirebaseFirestoreSwift
struct Book:Codable, Identifiable{
    
    @DocumentID var id:String?
    let title:String
    let profileId:String

}
