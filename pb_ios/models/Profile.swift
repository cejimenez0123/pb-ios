//
//  Profile.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import Foundation
import FirebaseFirestoreSwift
struct Profile:Codable{
    @DocumentID var id:String?
    let userId:String
    let username:String
    var profilePicture:String
    var selfStatement:String
    var privacy:Bool
    var bookmarkLibraryId: String
    var homeLibraryId:String

}
