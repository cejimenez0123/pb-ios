//
//  Library.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import Foundation
import FirebaseFirestoreSwift

struct Library:Codable,Identifiable{
   @DocumentID var id:String?
    let name:String
    let profileId:String
    init(id: String, name: String, profileId: String) {
        self.id = id
        self.name = name
        self.profileId = profileId
    }
}
