//
//  DashboardScreen.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import SwiftUI

struct DashboardScreen: View {
    
   @EnvironmentObject var viewModel:DashboardViewModel
    var body: some View {
        VStack{
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            LazyVStack(content: {
                ForEach(viewModel.pages,content: {(page) in
                    DashboardPageLazyItem(page: page)
                    
                })
            }).scaledToFill()
        }
       
    }
}

struct DashboardScreen_Previews: PreviewProvider {
            
    static var previews: some View {
       
        DashboardScreen()
    }
}
