//
//  DashboardViewModel.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import Foundation
import FirebaseAuth
import FirebaseFirestoreSwift
class DashboardViewModel:ObservableObject{
    @Published var currentUser:User? = nil
    @Published var myProfile:Profile? = nil
    
    @Published var pages = Array<Page>()
    private var mainRepo:UserRepository
    private var pageRepo:PageRepository
    init(currentUser: User? = nil, myProfile: Profile? = nil, mainRepo: UserRepository,pageRepo:PageRepository) {
        self.currentUser = currentUser
        self.myProfile = myProfile
        self.mainRepo = mainRepo
        self.pageRepo = pageRepo
        
        
        let uid = Auth.auth().currentUser?.uid
        
        if (uid != nil) {
          
                
          
              getUser(uId: uid!, ifSuccess: {
                    (user) in
                    
                    self.currentUser = user
                    
                  self.getProfile(profileId: user.defaultProfileId, ifSuccess:{(prof) in
                      self.myProfile=prof
                  })
                })
              
            }
            
        }
func getUser(uId:String,ifSuccess: @escaping (User)->Void){
    Task.init(operation: {
        
        mainRepo.getUser(uId: uId, ifSuccess: ifSuccess)
    })
    }
    
    func getProfile(profileId:String,ifSuccess: @escaping (Profile)->Void){
        Task.init(operation: {
            
            mainRepo.getProfile(profileId:profileId,ifSuccess:ifSuccess,ifError:{
                (e)in
                print(e)
            })
        })
    }
    func getPublicPages(){
        Task.init(operation: {
            pageRepo.getPublicPages(ifSuccess: {
                (pageList) in
                self.pages = pageList
                
            }, ifError: {
                (e) in
                print(e)
            })})
    }
}
