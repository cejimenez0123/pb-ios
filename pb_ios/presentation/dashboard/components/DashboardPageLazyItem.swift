//
//  DashboardPageLazyItem.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import SwiftUI

struct DashboardPageLazyItem: View {
    let page:Page
    
    var body: some View {
        VStack{
            if(page.type==PageType.text){
                TextPage(data: page.data)
            }
            }
        }}
    


struct DashboardPageLazyItem_Previews: PreviewProvider {
   
    static var previews: some View {
        DashboardPageLazyItem(page: Page(id:"",data:"", title: "Something gooood woohooo",type: "WOOO",profileId: "html/text",approvalScore:0,privacy: false))
    }
}
