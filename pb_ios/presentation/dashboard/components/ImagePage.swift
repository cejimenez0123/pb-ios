//
//  ImagePage.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/6/23.
//

import SwiftUI

struct ImagePage: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ImagePage_Previews: PreviewProvider {
    static var previews: some View {
        ImagePage()
    }
}
