//
//  pb_iosApp.swift
//  pb_ios
//
//  Created by Christian Jimenez on 3/5/23.
//

import SwiftUI
import FirebaseCore
@main
struct pb_iosApp: App {
    let persistenceController = PersistenceController.shared
    @StateObject var viewModelFactory = ViewModelFactory()

    init(){
        FirebaseApp.configure()
    }
    var body: some Scene {
        WindowGroup {
            NavigationView(content: {
                DashboardScreen()
            })
          .environmentObject(viewModelFactory.makeDashboardViewModel())
//            ContentView()
//
//                .environment(\.managedObjectContext, persistenceController.container.viewContext)
               
        }
        
    }
       
}
